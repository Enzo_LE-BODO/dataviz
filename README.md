# Présentation du projet

Le projet à pour objectif de suivre et de valoriser des données de
suivi des températures d'eau de mer du Golfe du Morbihan. Ce suivi
pourra ainsi être intégré à l'Observatoire des effets du changement
climatique porté par Golfe du Morbihan Vannes Agglomération (GMVA).

Dans ce projet, il s'agit de développer une application Web
(responsive) pour visualiser les données qui seront collectées par les
capteurs de température. Les données devront être présentées de
manière simple pour que celles-ci puissent être facilement comprises
par les habitants de l'agglomération. Les données pourront être
présentées via des graphiques et des tableaux. Les dernières données
collectées et un historique des données devront être présentés. Les
données collectées devront être stockées dans une base de données.


